// @ts-check
//
// The line above enables type checking for this file. Various IDEs interpret
// the @ts-check directive. It will give you helpful autocompletion when
// implementing this exercise.

/**
 * Determines how long it takes to prepare a certain juice.
 *
 * @param {string} name
 * @returns {number} time in minutes
 */
export function timeToMixJuice(name) {
let time = 0
switch (name) {
  case "Pure Strawberry Joy":
    time = 0.5
    break;
  case "Energizer":
    time = 1.5
    break;
  case "Green Garden":
    time = 1.5
    break;
  case "Energizer":
    time = 1.5
    break;
  case "Tropical Island":
    time = 3
    break;
  case "All or Nothing":
    time = 5
  break;
  default:
    time = 2.5
  }
return time
}

/**
 * Calculates the number of limes that need to be cut
 * to reach a certain supply.
 *
 * @param {number} wedgesNeeded
 * @param {string[]} limes
 * @returns {number} number of limes cut
 */
export function limesToCut(wedgesNeeded, limes) {
  if (wedgesNeeded === 0) {
    return 0;
  }
  let currentWedges = 0
  let limesCut = 0
  const smallLime = 6
  const mediumLime = 8
  const largeLime = 10
  while (currentWedges < wedgesNeeded) { 
    switch (limes[limesCut]) {
      case "small":
        currentWedges += smallLime
        limesCut++
      break;
      case "medium":
        currentWedges += mediumLime
        limesCut++
      break;
      case "large":
        currentWedges += largeLime
        limesCut++
      break;
      default:
      return limesCut
  }
} 
return limesCut
}

/**
 * Determines which juices still need to be prepared after the end of the shift.
 *
 * @param {number} timeLeft
 * @param {string[]} orders
 * @returns {string[]} remaining orders after the time is up
 */
export function remainingOrders(timeLeft, orders) {
  while (timeLeft > 0) {
    timeLeft -= timeToMixJuice(orders[0])
    orders.shift();
  }
  return orders
}